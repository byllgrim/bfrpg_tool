# BFRPG Tool

A single html document to assist in playing BFRPG

DISCLAIMER:
This software is incomplete, I don't know webdev, and I just wrote
this while on the train.

## Goals

* Manage a characters information
  * Name, Backstory, Abilities, Inventory, etc
* Guide through steps of character creation
  * Rolling, Race, Class, Purchases, etc
* Run on linux and winblows
* Usable without installing anything
  * HTML and JavaScript is ubiquitous, right?
* Assist in leveling up
* Really fucking easy to use
